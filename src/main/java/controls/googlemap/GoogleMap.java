package controls.googlemap;

import dbmanager.tables.Station;
import dbmanager.tables.Station_type;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.concurrent.Worker;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

import java.util.ArrayList;
import java.util.List;

public class GoogleMap extends Parent {

    private ArrayList<Station> stationList;

    public GoogleMap() {
        initMap();
        initCommunication();
        getChildren().add(webView);
        setMapCenter(41, 69);


    }

    public void updateMap(){
        String title;
        String label;
        int lat=38;
        int lon=68;
        for(Station station :stationList){
//            title="'Lat=" + station.getLatitude() +
//                    " Lon=" + station.getLongitude() +
//                    " Network=" + station.getNetwork() +
//                    " Station=" + station.getStationcode()+"'";
//            label="'"+station.getNetwork() + "." + station.getStationcode()+"'";
//            addMarker(title,label,station.getLatitude(),station.getLongitude());

            title="'"+station.getName().replace("'","\\'")+"'";
            label="'"+station.getCode()+"'";
            addMarker(title,label,lat,lon);
            lat++;lon++;
        }
    }

    private void initMap() {
        webView = new WebView();
        webEngine = webView.getEngine();
        webEngine.load(getClass().getResource("/html/map.html").toExternalForm());
        ready = false;
        webEngine.getLoadWorker().stateProperty().addListener((observableValue, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED)
            {
                ready = true;
            }
        });
    }

    private void initCommunication() {
        webEngine.getLoadWorker().stateProperty().addListener((observableValue, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED)
            {
                doc = (JSObject) webEngine.executeScript("window");
                doc.setMember("app", GoogleMap.this);
            }
        });
    }

    private void addMarker(String markerTitle,String markerLabel,double lat,double lng){
        String sLat=Double.toString(lat);
        String sLng=Double.toString(lng);
        invokeJS("addMarker("+markerTitle+", "+markerLabel +", "+sLat+", "+sLng+")");
    }

    private void invokeJS(final String str) {
        if(ready) {
            doc.eval(str);
        }
        else {
            webEngine.getLoadWorker().stateProperty().addListener((observableValue, oldState, newState) -> {
                if (newState == Worker.State.SUCCEEDED)
                {
                    doc.eval(str);
                }
            });
        }
    }

    public void setOnMapLatLngChanged(EventHandler<MapEvent> eventHandler) {
        onMapLatLngChanged = eventHandler;
    }

    public void handle(double lat, double lng) {
        if(onMapLatLngChanged != null) {
            MapEvent event = new MapEvent(this, lat, lng);
            onMapLatLngChanged.handle(event);
        }
    }

    public void setMarkerPosition(double lat, double lng) {
        String sLat = Double.toString(lat);
        String sLng = Double.toString(lng);
        invokeJS("setMarkerPosition(" + sLat + ", " + sLng + ")");
    }

    public void setMapCenter(double lat, double lng) {
        String sLat = Double.toString(lat);
        String sLng = Double.toString(lng);
        invokeJS("setMapCenter(" + sLat + ", " + sLng + ")");
    }

    public void switchSatellite() {
        invokeJS("switchSatellite()");
    }

    public void switchRoadmap() {
        invokeJS("switchRoadmap()");
    }

    public void switchHybrid() {
        invokeJS("switchHybrid()");
    }

    public void switchTerrain() {
        invokeJS("switchTerrain()");
    }

    public void startJumping() {
        invokeJS("startJumping()");
    }

    public void stopJumping() {
        invokeJS("stopJumping()");
    }

    public void setHeight(double h) {
        webView.setPrefHeight(h);
    }

    public void setWidth(double w) {
        webView.setPrefWidth(w);
    }

    public ReadOnlyDoubleProperty widthProperty() {
        return webView.widthProperty();
    }

    private JSObject doc;
    private EventHandler<MapEvent> onMapLatLngChanged;
    private WebView webView;
    private WebEngine webEngine;
    private boolean ready;

    public ArrayList<Station> getStationList() {
        return stationList;
    }

    public void setStationList(ArrayList<Station> stationList) {
        this.stationList = stationList;
    }
}
