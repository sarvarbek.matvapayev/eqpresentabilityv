package controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import controls.googlemap.GoogleMap;
import dbmanager.tables.Station;
import dbmanager.tables.Station_type;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    @FXML
    private AnchorPane bodyAP;

    @FXML
    private AnchorPane splitUpAP;

    @FXML
    private AnchorPane splitDownAP;

    @FXML
    private JFXHamburger hamburger;

    @FXML
    private JFXDrawer drawer;


    public void initialize(URL location, ResourceBundle resources) {
        try {
            DrawerController controller = new DrawerController();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/drawer_form.fxml"));
            loader.setController(controller);
            AnchorPane pane = loader.load();
            drawer.setSidePane(pane);
            final HamburgerBackArrowBasicTransition burgerTask = new HamburgerBackArrowBasicTransition(hamburger);
            burgerTask.setRate(-1);
            hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
                burgerTask.setRate(burgerTask.getRate() * -1);
                burgerTask.play();
                if (drawer.isOpened()) {
                    drawer.close();
                } else {
                    drawer.open();

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<Station> stations = Station.getStation();

        GoogleMap googleMap = new GoogleMap();
        googleMap.setStationList(stations);
        splitUpAP.getChildren().add(googleMap);
        splitUpAP.widthProperty().addListener(event -> googleMap.setWidth(splitUpAP.getWidth()));
        splitUpAP.heightProperty().addListener(event -> googleMap.setHeight(splitUpAP.getHeight()));
        googleMap.updateMap();


//        satelliteMI.setOnAction(event -> googleMap.switchSatellite());
//        hybridMI.setOnAction(event -> googleMap.switchHybrid());
//        roadmapMI.setOnAction(event -> googleMap.switchRoadmap());
//        terrainMI.setOnAction(event -> googleMap.switchTerrain());
    }
}

class BindCheckBoxStationType{
    private JFXCheckBox checkBox;
    private Station_type station_type;

    public BindCheckBoxStationType(JFXCheckBox checkBox,Station_type station_type){
        this.checkBox=checkBox;
        this.station_type=station_type;
    }

    public JFXCheckBox getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(JFXCheckBox checkBox) {
        this.checkBox = checkBox;
    }

    public Station_type getStation_type() {
        return station_type;
    }

    public void setStation_type(Station_type station_type){
        this.station_type = station_type;
    }
}
