package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import dbmanager.tables.Station_type;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class DrawerController implements Initializable {

    @FXML
    private VBox stationTypeVBOX;

    @FXML
    private JFXButton updateMapBTN;

    private Map<JFXCheckBox,Station_type> bindCST;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        ArrayList<Station_type> station_typeList=Station_type.select();
        bindCST=new HashMap<>();
        for(Station_type item : station_typeList){
            JFXCheckBox checkBox=new JFXCheckBox();
            checkBox.setText(item.getName());
            checkBox.setSelected(true);
            bindCST.put(checkBox,item);
            stationTypeVBOX.getChildren().add(checkBox);

        }
    }
}
