import controller.MainController;
import dbmanager.DBManager;
import dbmanager.tables.Apparatus_type;
import dbmanager.tables.Station;
import dbmanager.tables.Station_type;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public void start(Stage primaryStage) throws Exception {
        loadDatas();
        MainController controller=new MainController();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
        loader.setController(controller);
        Parent root = loader.load();
        primaryStage.setScene((new Scene(root)));
        primaryStage.setMaximized(true);
        primaryStage.setTitle("EARTHQUAKE MONITORING");
        //primaryStage.getIcons().add(new Image(getClass().getResource("/images/ico.png").toString()));
        primaryStage.show();
    }

    public void loadDatas(){
        DBManager.checkTable(Apparatus_type.class);
        DBManager.connect();
        Apparatus_type a=new Apparatus_type();
        boolean answer=a.isExistsTable();
        if(answer){
            System.out.println(a.getTableName()+" exist");
        }
        else {
            System.out.println(a.getTableName()+" not exist. Creating...");
            if(a.createTable()){
                Apparatus_type.runSeeder();
                System.out.println("created");
            }else {
                System.out.println("Not created");
            }
        }

        Station station=new Station();
        answer=station.isExistsTable();
        if(answer){
            System.out.println(station.getTableName()+" exist");
        } else {
            System.out.println(station.getTableName()+" not exist. Creating ...");
            if(station.createTable()){
                Station.runSeeder();
                System.out.println("created");
            }
            else {
                System.out.println("Not created");
            }
        }


        Station_type type=new Station_type();
        answer=type.isExistsTable();
        if(answer){
            System.out.println(type.getTableName()+" exist");
        } else {
            System.out.println(type.getTableName()+" not existing. Creating...");
            if(type.createTable()){
                Station_type.runSeeder();
                System.out.println(type.getTableName()+ " created, seeder run");
            }else {
                System.out.println(type.getTableName()+ " not created");
            }
        }

    }

    @Override
    public void stop() throws Exception {
        super.stop();
        DBManager.close();
    }
}
