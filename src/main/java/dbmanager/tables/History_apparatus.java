package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;
import javafx.scene.control.Tab;

import java.text.SimpleDateFormat;
import java.util.Date;

public class History_apparatus extends Table {
    public static final String TABLE_NAME = "HISTORY_APPARATUS";

    public static final String ID_COL_NAME = "id";
    public static final String APPARATUS_ID_COL_NAME = "apparatus_id";
    public static final String DATE_COL_NAME = "Date";
    public static final String FROM_PLACE_ID_COL_NAME = "from_place_id";
    public static final String TO_PLACE_ID_COL_NAME = "to_place_id";
    public static final String STATION_ID_COL_NAME = "station_id";


    private int id;
    private int apparatus_id;
    private Date date;
    private int from_place_id;
    private int to_place_id;
    private int station_id;
    private SimpleDateFormat dateFormat;

    public History_apparatus() {
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    }

    public History_apparatus(int apparatus_id, Date date, int from_place_id, int to_place_id, int station_id) {
        this.apparatus_id = apparatus_id;
        this.date = date;
        this.from_place_id = from_place_id;
        this.to_place_id = to_place_id;
        this.station_id = station_id;
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    }

    public History_apparatus(int id, int apparatus_id, Date date, int from_place_id, int to_place_id, int station_id) {
        this.id = id;
        this.apparatus_id = apparatus_id;
        this.date = date;
        this.from_place_id = from_place_id;
        this.to_place_id = to_place_id;
        this.station_id = station_id;
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                APPARATUS_ID_COL_NAME + " integer," +
                DATE_COL_NAME + " date," +
                FROM_PLACE_ID_COL_NAME + " integer," +
                TO_PLACE_ID_COL_NAME + " integer," +
                STATION_ID_COL_NAME + " integer" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query = "SELECT * FROM " + TABLE_NAME + " LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                APPARATUS_ID_COL_NAME + "," +
                DATE_COL_NAME + "," +
                FROM_PLACE_ID_COL_NAME + "," +
                TO_PLACE_ID_COL_NAME + "," +
                STATION_ID_COL_NAME +
                ") VALUES (" +
                this.getApparatus_id() + ",\'" +
                dateFormat.format(this.getDate()).replace("'", "''").replace("\\", "/") + "\'," +
                this.getFrom_place_id() + "," +
                this.getTo_place_id() + "," +
                this.getStation_id() +
                ");";
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                ID_COL_NAME + "," +
                APPARATUS_ID_COL_NAME + "," +
                DATE_COL_NAME + "," +
                FROM_PLACE_ID_COL_NAME + "," +
                TO_PLACE_ID_COL_NAME + "," +
                STATION_ID_COL_NAME +
                ") VALUES (" +
                id + "," +
                this.getApparatus_id() + ",\'" +
                dateFormat.format(this.getDate()).replace("'", "''").replace("\\", "/") + "\'," +
                this.getFrom_place_id() + "," +
                this.getTo_place_id() + "," +
                this.getStation_id() +
                ");";
        DBManager.execute(query);
    }

    public static void runSeeder(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApparatus_id() {
        return apparatus_id;
    }

    public void setApparatus_id(int apparatus_id) {
        this.apparatus_id = apparatus_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getFrom_place_id() {
        return from_place_id;
    }

    public void setFrom_place_id(int from_place_id) {
        this.from_place_id = from_place_id;
    }

    public int getTo_place_id() {
        return to_place_id;
    }

    public void setTo_place_id(int to_place_id) {
        this.to_place_id = to_place_id;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }
}
