package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

public class Region extends Table {
    public static final String TABLE_NAME = "REGION";

    public static final String ID_COL_NAME = "id";
    public static final String NAME_COL_NAME = "Name";

    private int id;
    private String name;

    public Region() {
    }

    public Region(String name) {
        this.name = name;
    }

    public Region(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                NAME_COL_NAME + " text" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query = "SELECT * FROM " + TABLE_NAME + " LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query = "INSERT INTO " + this.getTableName() + " (" + NAME_COL_NAME + ")" +
                " VALUES (\'" + this.getName().replace("'", "''").replace("\\", "/") + "\');";
        //System.out.println(query);
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" + ID_COL_NAME + ", " + NAME_COL_NAME + ")" +
                " VALUES (" + id + ",\'" + this.getName().replace("'", "''").replace("\\", "/") + "\');";
        //System.out.println(query);
        DBManager.execute(query);
    }

    public static void runSeeder(){
        Region region1=new Region(1,"Qoraqalpog'iston");
        region1.insert(region1.getId());
        Region region2=new Region(2,"Xorazm");
        region2.insert(region2.getId());
        Region region3=new Region(3,"Buxoro");
        region3.insert(region3.getId());
        Region region4=new Region(4,"Navoiy");
        region4.insert(region4.getId());
        Region region5=new Region(5,"Samarqand");
        region5.insert(region5.getId());
        Region region6=new Region(6,"Jizzax");
        region6.insert(region6.getId());
        Region region7=new Region(7,"Sirdaryo");
        region7.insert(region7.getId());
        Region region8=new Region(8,"Qashqadaryo");
        region8.insert(region8.getId());
        Region region9=new Region(9,"Toshkent viloyati");
        region9.insert(region9.getId());
        Region region10=new Region(10,"Toshkent shahri");
        region10.insert(region10.getId());
        Region region11=new Region(11,"Namangan");
        region11.insert(region11.getId());
        Region region12=new Region(12,"Farg'ona");
        region12.insert(region12.getId());
        Region region13=new Region(13,"Andijon");
        region13.insert(region13.getId());
        Region region14=new Region(14,"Surxandaryo");
        region14.insert(region14.getId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
