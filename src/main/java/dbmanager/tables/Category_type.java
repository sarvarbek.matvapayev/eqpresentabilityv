package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

public class Category_type extends Table {
    public static final String TABLE_NAME="CATEGORY_TYPE";
    public static final String ID_COL_NAME="id";
    public static final String NAME_COL_NAME="Name";


    private int id;
    private String name;

    public Category_type() {
    }

    public Category_type(String name) {
        this.name = name;
    }

    public Category_type(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query="CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                NAME_COL_NAME+" text" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query="SELECT * FROM "+TABLE_NAME+" LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query="INSERT INTO "+this.getTableName()+" ("+NAME_COL_NAME+")"+
                " VALUES (\'"+this.getName().replace("'","''").replace("\\","/")+"\');";
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" + ID_COL_NAME + ", " + NAME_COL_NAME + ")" +
                " VALUES (" + id + ",\'" + this.getName().replace("'", "''").replace("\\", "/") + "\');";
        DBManager.execute(query);
    }

    public static void runSeeder(){
        Category_type type1=new Category_type(1,"1-category");
        type1.insert(type1.getId());
        Category_type type2=new Category_type(2,"2-category");
        type2.insert(type2.getId());
        Category_type type3=new Category_type(3,"3-category");
        type3.insert(type3.getId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
