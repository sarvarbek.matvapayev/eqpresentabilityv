package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

public class Station_type_station extends Table {
    public static final String TABLE_NAME = "STATION_TYPE_STATION";

    public static final String ID_COL_NAME = "id";
    public static final String STATION_ID_COL_NAME = "station_id";
    public static final String TYPE_ID_COL_NAME = "type_id";

    private int id;
    private int station_id;
    private int type_id;

    public Station_type_station() {
    }

    public Station_type_station(int station_id, int type_id) {
        this.station_id = station_id;
        this.type_id = type_id;
    }

    public Station_type_station(int id, int station_id, int type_id) {
        this.id = id;
        this.station_id = station_id;
        this.type_id = type_id;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                STATION_ID_COL_NAME + " integer," +
                TYPE_ID_COL_NAME + " integer" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query = "SELECT * FROM " + TABLE_NAME + " LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                STATION_ID_COL_NAME + "," +
                TYPE_ID_COL_NAME +
                ") VALUES (" +
                this.getStation_id() + "," +
                this.getType_id() +
                ");";
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                ID_COL_NAME + "," +
                STATION_ID_COL_NAME + "," +
                TYPE_ID_COL_NAME +
                ") VALUES (" +
                id + "," +
                this.getStation_id() + "," +
                this.getType_id() +
                ");";
        DBManager.execute(query);
    }

    public static void runSeeder(){
        Station_type_station typeStation1=new Station_type_station(1,1,1);
        typeStation1.insert(typeStation1.getId());
        Station_type_station typeStation2=new Station_type_station(2,2,1);
        typeStation2.insert(typeStation2.getId());
        Station_type_station typeStation3=new Station_type_station(3,2,2);
        typeStation3.insert(typeStation3.getId());
        Station_type_station typeStation4=new Station_type_station(4,2,3);
        typeStation4.insert(typeStation4.getId());
        Station_type_station typeStation5=new Station_type_station(5,3,1);
        typeStation5.insert(typeStation5.getId());
        Station_type_station typeStation6=new Station_type_station(6,3,3);
        typeStation6.insert(typeStation6.getId());
        Station_type_station typeStation7=new Station_type_station(7,4,1);
        typeStation7.insert(typeStation7.getId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }
}
