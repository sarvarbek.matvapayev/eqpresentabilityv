package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

public class Apparatus_type extends Table {
    public static final String TABLE_NAME = "APPARATUS_TYPE";

    public static final String ID_COL_NAME = "id";
    public static final String NAME_COL_NAME = "Name";


    private int id;
    private String name;

    public Apparatus_type(String name) {
        this.name = name;
    }

    public Apparatus_type(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Apparatus_type() {
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                NAME_COL_NAME + " text" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query = "SELECT * FROM " + TABLE_NAME + " LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query = "INSERT INTO " + this.getTableName() + " (" + NAME_COL_NAME + ")" +
                " VALUES (\'" + this.getName().replace("'", "''").replace("\\", "/") + "\');";
        //System.out.println(query);
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" + ID_COL_NAME + ", " + NAME_COL_NAME + ")" +
                " VALUES (" + id + ",\'" + this.getName().replace("'", "''").replace("\\", "/") + "\');";
        //System.out.println(query);
        DBManager.execute(query);
    }

    public static void runSeeder() {
        Apparatus_type apparatus_type1 = new Apparatus_type(1,"Industrial Computer, Mec 5002");
        apparatus_type1.insert(apparatus_type1.getId());
        Apparatus_type apparatus_type2 = new Apparatus_type(2,"Industrial Computer, BBox 5230");
        apparatus_type2.insert(apparatus_type2.getId());
        Apparatus_type apparatus_type3 = new Apparatus_type(3,"Industrial Computer, ARK 1123");
        apparatus_type3.insert(apparatus_type3.getId());
        Apparatus_type apparatus_type4 = new Apparatus_type(4,"Digitizer, Webtronics 3K");
        apparatus_type4.insert(apparatus_type4.getId());
        Apparatus_type apparatus_type5 = new Apparatus_type(5,"Digitizer, Webtronics 6K");
        apparatus_type5.insert(apparatus_type5.getId());
        Apparatus_type apparatus_type6 = new Apparatus_type(6,"Seismometr, CME 3311");
        apparatus_type6.insert(apparatus_type6.getId());
        Apparatus_type apparatus_type7 = new Apparatus_type(7,"Seismometr, Guralp 6T");
        apparatus_type7.insert(apparatus_type7.getId());
        Apparatus_type apparatus_type8 = new Apparatus_type(8,"Seismometr, Accelerometer 54T");
        apparatus_type8.insert(apparatus_type8.getId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
