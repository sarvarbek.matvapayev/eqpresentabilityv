package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

import java.sql.ResultSet;
import java.util.ArrayList;

public class Station_type extends Table {
    public static final String TABLE_NAME="STATION_TYPE";
    public static final String ID_COL_NAME="id";
    public static final String NAME_COL_NAME="Name";


    private int id;
    private String name;

    public Station_type(String name) {
        this.name = name;
    }

    public Station_type(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Station_type() {
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query="CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                NAME_COL_NAME+" text" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query="SELECT * FROM "+TABLE_NAME+" LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert(){
        String query="INSERT INTO "+this.getTableName()+" ("+Apparatus_type.NAME_COL_NAME+")"+
                " VALUES (\'"+this.getName().replace("'","''").replace("\\","/")+"\');";
        //System.out.println(query);
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" + ID_COL_NAME + ", " + NAME_COL_NAME + ")" +
                " VALUES (" + id + ",\'" + this.getName().replace("'", "''").replace("\\", "/") + "\');";
        //System.out.println(query);
        DBManager.execute(query);
    }

    public static void runSeeder(){
        Station_type type1=new Station_type(1,"Seismological");
        type1.insert(type1.getId());
        Station_type type2=new Station_type(2,"Hydrogeological");
        type2.insert(type2.getId());
        Station_type type3=new Station_type(3,"Geophysical");
        type3.insert(type3.getId());
    }

    public static ArrayList<Station_type> select(){
        String query="SELECT * FROM "+TABLE_NAME;
        ArrayList<Station_type> station_types=new ArrayList<>();
        ResultSet resultSet=DBManager.executeQuery(query);
        if(resultSet!=null){
            try {
                Station_type station_type=null;
                while (resultSet.next()){
                    station_type=new Station_type();
                    station_type.setId(resultSet.getInt(ID_COL_NAME));
                    station_type.setName(resultSet.getString(NAME_COL_NAME));
                    station_types.add(station_type);
                }
            }catch (Exception ex){
                ex.printStackTrace();
                //TODO exception form need show
            }
        }
        return station_types;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
