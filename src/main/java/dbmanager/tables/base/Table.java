package dbmanager.tables.base;

public abstract class Table {
    public abstract String getTableName();
    public abstract boolean createTable();
    public abstract boolean isExistsTable();
    public abstract void insert();
}
