package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Station extends Table {
    public static final String TABLE_NAME = "STATION";

    public static final String ID_COL_NAME = "id";
    public static final String NAME_COL_NAME = "Name";
    public static final String CODE_COL_NAME = "Code";
    public static final String CATEGORY_ID_COL_NAME = "category_id";
    public static final String PRIMING_ID_COL_NAME = "priming_id";
    public static final String COMMUNICATION_ID_COL_NAME = "communication_id";
    public static final String PARTIA_ID_COL_NAME = "partia_id";
    public static final String OPEN_DATE_COL_NAME = "Open_date";
    public static final String WORK_DATE_COL_NAME = "Work_date";
    public static final String DESCRIPTION_COL_NAME = "Description";


    private int id;
    private String name;
    private String code;
    private int category_id;
    private int priming_id;
    private int communication_id;
    private int partia_id;
    private Date open_date;
    private Date work_date;
    private String description;

    private SimpleDateFormat dateFormat;

    public Station() {
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    }

    public Station(String name, String code, int category_id, int priming_id, int communication_id, int partia_id, Date open_date, Date work_date, String description) {
        this.name = name;
        this.code = code;
        this.category_id = category_id;
        this.priming_id = priming_id;
        this.communication_id = communication_id;
        this.partia_id = partia_id;
        this.open_date = open_date;
        this.work_date = work_date;
        this.description = description;
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    }

    public Station(int id, String name, String code, int category_id, int priming_id, int communication_id, int partia_id, Date open_date, Date work_date, String description) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.category_id = category_id;
        this.priming_id = priming_id;
        this.communication_id = communication_id;
        this.partia_id = partia_id;
        this.open_date = open_date;
        this.work_date = work_date;
        this.description = description;
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                NAME_COL_NAME + " text," +
                CODE_COL_NAME + " text," +
                CATEGORY_ID_COL_NAME + " integer," +
                PRIMING_ID_COL_NAME + " integer," +
                COMMUNICATION_ID_COL_NAME + " integer," +
                PARTIA_ID_COL_NAME + " integer," +
                OPEN_DATE_COL_NAME + " date," +
                WORK_DATE_COL_NAME + " date," +
                DESCRIPTION_COL_NAME + " text" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query = "SELECT * FROM " + TABLE_NAME + " LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                NAME_COL_NAME + "," +
                CODE_COL_NAME + "," +
                CATEGORY_ID_COL_NAME + "," +
                PRIMING_ID_COL_NAME + "," +
                COMMUNICATION_ID_COL_NAME + "," +
                PARTIA_ID_COL_NAME + "," +
                OPEN_DATE_COL_NAME + "," +
                WORK_DATE_COL_NAME + "," +
                DESCRIPTION_COL_NAME +
                ") VALUES (\'" +
                this.getName() + "\',\'" +
                this.getCode() + "\'," +
                this.getCategory_id() + "," +
                this.getPriming_id() + "," +
                this.getCommunication_id() + "," +
                this.getPartia_id() + ",\'" +
                dateFormat.format(this.getOpen_date()).replace("'", "''").replace("\\", "/") + "\',\'" +
                dateFormat.format(this.getWork_date()).replace("'", "''").replace("\\", "/") + "\',\'" +
                this.getDescription().replace("'", "''").replace("\\", "/") + "\'" +
                ");";
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                ID_COL_NAME + "," +
                NAME_COL_NAME + "," +
                CODE_COL_NAME + "," +
                CATEGORY_ID_COL_NAME + "," +
                PRIMING_ID_COL_NAME + "," +
                COMMUNICATION_ID_COL_NAME + "," +
                PARTIA_ID_COL_NAME + "," +
                OPEN_DATE_COL_NAME + "," +
                WORK_DATE_COL_NAME + "," +
                DESCRIPTION_COL_NAME +
                ") VALUES (\'" +
                id + "," +
                this.getName() + "\',\'" +
                this.getCode() + "\'," +
                this.getCategory_id() + "," +
                this.getPriming_id() + "," +
                this.getCommunication_id() + "," +
                this.getPartia_id() + ",\'" +
                dateFormat.format(this.getOpen_date()).replace("'", "''").replace("\\", "/") + "\',\'" +
                dateFormat.format(this.getWork_date()).replace("'", "''").replace("\\", "/") + "\',\'" +
                this.getDescription().replace("'", "''").replace("\\", "/") + "\'" +
                ");";
        DBManager.execute(query);
    }

    public static void runSeeder() throws ParseException {
        DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
        Station station1=new Station(1,"Andijon","AND",2,3,1,2,dateFormat.parse("12-12-2000"),dateFormat.parse("12-12-2001"),"Partia boshlig'i Qodr Qo'chqorov");
        station1.insert(station1.getId());
        Station station2=new Station(2,"Toshkent","TAS",1,1,2,1,dateFormat.parse("12-12-1901"),dateFormat.parse("12-12-1902"),"Partia boshlig'i Sherzod Abdunabiyev");
        station2.insert(station2.getId());
        Station station3=new Station(3,"Mingtut","MT",2,3,3,2,dateFormat.parse("12-12-2014"),dateFormat.parse("12-12-2014"),"Partia boshlig'i Qodr Qo'chqorov");
        station3.insert(station3.getId());
        Station station4=new Station(4,"Chimgan","CHMG",1,1,4,1,dateFormat.parse("12-12-2010"),dateFormat.parse("12-12-2011"),"Partia boshlig'i Sherzod Abdunabiyev");
        station4.insert(station4.getId());
    }

    public static ArrayList<Station> getStation() {
        String query = "SELECT * FROM " + TABLE_NAME;

        ArrayList<Station> stationArrayList = new ArrayList<>();
        ResultSet resultSet = DBManager.executeQuery(query);
        if (resultSet != null) {
            try {
                Station station = null;
                while (resultSet.next()) {
                    station = new Station();
                    station.setId(resultSet.getInt(ID_COL_NAME));
                    station.setCode(resultSet.getString(CODE_COL_NAME));
                    station.setName(resultSet.getString(NAME_COL_NAME));
                    stationArrayList.add(station);
                }
            } catch (Exception e) {
                e.printStackTrace();
                //TODO exception form need show
            }
        }
        return stationArrayList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getPriming_id() {
        return priming_id;
    }

    public void setPriming_id(int priming_id) {
        this.priming_id = priming_id;
    }

    public int getCommunication_id() {
        return communication_id;
    }

    public void setCommunication_id(int communication_id) {
        this.communication_id = communication_id;
    }

    public int getPartia_id() {
        return partia_id;
    }

    public void setPartia_id(int partia_id) {
        this.partia_id = partia_id;
    }

    public Date getOpen_date() {
        return open_date;
    }

    public void setOpen_date(Date open_date) {
        this.open_date = open_date;
    }

    public Date getWork_date() {
        return work_date;
    }

    public void setWork_date(Date work_date) {
        this.work_date = work_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
