package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

public class Partia extends Table {
    public static final String TABLE_NAME = "PARTIA";

    public static final String ID_COL_NAME = "id";
    public static final String NAME_COL_NAME = "Name";


    private int id;
    private String name;

    public Partia() {
    }

    public Partia(String name) {
        this.name = name;
    }

    public Partia(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                NAME_COL_NAME + " text" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query = "SELECT * FROM " + TABLE_NAME + " LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query = "INSERT INTO " + this.getTableName() + " (" + NAME_COL_NAME + ")" +
                " VALUES (\'" + this.getName().replace("'", "''").replace("\\", "/") + "\');";
        //System.out.println(query);
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" + ID_COL_NAME + ", " + NAME_COL_NAME + ")" +
                " VALUES (" + id + ",\'" + this.getName().replace("'", "''").replace("\\", "/") + "\');";
        //System.out.println(query);
        DBManager.execute(query);
    }

    public static void runSeeder(){
        Partia  partia1=new Partia(1,"Toshkent oldi hududiy partiyasi");//Sherzod aka
        partia1.insert(partia1.getId());
        Partia  partia2=new Partia(2,"Sharqiy hududiy partiyasi");//Qodir aka
        partia2.insert(partia2.getId());
        Partia  partia3=new Partia(3,"Janubiy hududiy partiyasi");//Murod aka
        partia3.insert(partia3.getId());
        Partia  partia4=new Partia(4,"Samarqand hududiy partiyasi");//Obloqul aka
        partia4.insert(partia4.getId());
        Partia  partia5=new Partia(5,"G'arbiy hududiy partiyasi");//Xo'jam aka
        partia5.insert(partia5.getId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
