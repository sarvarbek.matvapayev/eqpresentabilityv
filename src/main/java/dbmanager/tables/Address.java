package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

public class Address extends Table {
    public static final String TABLE_NAME = "ADDRESS";

    public static final String ID_COL_NAME = "id";
    public static final String LAT_COL_NAME = "Latitude";
    public static final String LON_COL_NAME = "Longitude";
    public static final String DEPTH_COL_NAME = "Depth";
    public static final String REGION_ID_COL_NAME = "region_id";
    public static final String ADDRESS_ID_COL_NAME = "Superscription";
    public static final String STATION_ID_COL_NAME = "station_id";

    private int id;
    private double lat;
    private double lon;
    private double depth;
    private int region_id;
    private String superscription;//Address
    private int station_id;

    public Address() {
    }

    public Address(double lat, double lon, double depth, int region_id, String superscription, int station_id) {
        this.lat = lat;
        this.lon = lon;
        this.depth = depth;
        this.region_id = region_id;
        this.superscription = superscription;
        this.station_id = station_id;
    }

    public Address(int id, double lat, double lon, double depth, int region_id, String superscription, int station_id) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.depth = depth;
        this.region_id = region_id;
        this.superscription = superscription;
        this.station_id = station_id;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                LAT_COL_NAME + " real," +
                LON_COL_NAME + " real," +
                DEPTH_COL_NAME + " real," +
                REGION_ID_COL_NAME + " integer," +
                ADDRESS_ID_COL_NAME + " text," +
                STATION_ID_COL_NAME + " integer" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query = "SELECT * FROM " + TABLE_NAME + " LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                LAT_COL_NAME + "," +
                LON_COL_NAME + "," +
                DEPTH_COL_NAME + "," +
                REGION_ID_COL_NAME + "," +
                ADDRESS_ID_COL_NAME + "," +
                STATION_ID_COL_NAME +
                ") VALUES (" +
                Double.toString(this.getLat()).replace(",", ".") + "," +
                Double.toString(this.getLon()).replace(",", ".") + "," +
                Double.toString(this.getDepth()).replace(",", ".") + "," +
                this.getRegion_id() + ", \'" +
                this.getSuperscription().replace("'", "''").replace("\\", "/") + "\'," +
                this.getStation_id() +
                ");";
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                ID_COL_NAME + "," +
                LAT_COL_NAME + "," +
                LON_COL_NAME + "," +
                DEPTH_COL_NAME + "," +
                REGION_ID_COL_NAME + "," +
                ADDRESS_ID_COL_NAME + "," +
                STATION_ID_COL_NAME + "," +
                ") VALUES (" +
                id + "," +
                Double.toString(this.getLat()).replace(",", ".") + "," +
                Double.toString(this.getLon()).replace(",", ".") + "," +
                Double.toString(this.getDepth()).replace(",", ".") + "," +
                this.getRegion_id() + ", \'" +
                this.getSuperscription().replace("'", "''").replace("\\", "/") + "\'," +
                this.getStation_id() +
                ");";
        DBManager.execute(query);
    }

    public static void runSeeder(){
        Address address1=new Address(1,40.7549,72.3639,0.524,13,"Андижон вилояти, Андижон шахри, Мусаев кучаси 12 уй.",1);
        address1.insert(address1.getId());
        Address address2=new Address(2,41.3294,69.2858,0.1,10,"Тошкент шахар, Юнусобод тумани, Махсумов кучаси 85 уй.",2);
        address2.insert(address2.getId());
        Address address3=new Address(3,40.4659,70.9753,0,12,"Фаргона вилояти, Ўзбекистон тумани, Тулаш дашти, Мукумий поселкасидан утгач унг кулда мошина бозордан 500 метр утгандан унг кулга кирилади",3);
        address3.insert(address3.getId());
        Address address4=new Address(4,41.5512,70.0136,0.2,9,"Тошкент вилояти, Бўстонлиқ тумани, Чимган КФЙ, Кизил жар махалласи, охирги уй, Ракинур дам олиш оромгохи олдидан кирилади. ",4);
        address4.insert(address4.getId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }

    public int getRegion_id() {
        return region_id;
    }

    public void setRegion_id(int region_id) {
        this.region_id = region_id;
    }

    public String getSuperscription() {
        return superscription;
    }

    public void setSuperscription(String superscription) {
        this.superscription = superscription;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }
}
