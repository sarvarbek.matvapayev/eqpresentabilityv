package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

public class Apparatus extends Table {
    public static final String TABLE_NAME = "APPARATUS";

    public static final String ID_COL_NAME = "id";
    public static final String TYPE_ID_COL_NAME = "type_id";
    public static final String PLACE_ID_COL_NAME = "place_id";
    public static final String DESCRIPTION_COL_NAME = "Description";
    public static final String INV_COL_NAME = "INV";
    public static final String STATION_ID_COL_NAME = "station_id";


    private int id;
    private int type_id;
    private int place_id;
    private String description;
    private String inv;
    private int station_id;

    public Apparatus() {
    }

    public Apparatus(int type_id, int place_id, String description, String inv, int station_id) {
        this.type_id = type_id;
        this.place_id = place_id;
        this.description = description;
        this.inv = inv;
        this.station_id = station_id;
    }

    public Apparatus(int id, int type_id, int place_id, String description, String inv, int station_id) {
        this.id = id;
        this.type_id = type_id;
        this.place_id = place_id;
        this.description = description;
        this.inv = inv;
        this.station_id = station_id;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                TYPE_ID_COL_NAME + " integer," +
                PLACE_ID_COL_NAME + " integer," +
                DESCRIPTION_COL_NAME + " text," +
                INV_COL_NAME + " text," +
                STATION_ID_COL_NAME + " integer" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query = "SELECT * FROM " + TABLE_NAME + " LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                TYPE_ID_COL_NAME + "," +
                PLACE_ID_COL_NAME + "," +
                DESCRIPTION_COL_NAME + "," +
                INV_COL_NAME + "," +
                STATION_ID_COL_NAME +
                ") VALUES (" +
                this.getType_id() + ", " +
                this.getPlace_id() + ",\'" +
                this.getDescription().replace("'", "''").replace("\\", "/") + "\',\'" +
                this.getInv().replace("'", "''").replace("\\", "/") + "\'," +
                this.getStation_id() +
                ");";
        DBManager.execute(query);
    }

    public void insert(int id){
        String query = "INSERT INTO " + this.getTableName() + " (" +
                ID_COL_NAME + "," +
                TYPE_ID_COL_NAME + "," +
                PLACE_ID_COL_NAME + "," +
                DESCRIPTION_COL_NAME + "," +
                INV_COL_NAME + "," +
                STATION_ID_COL_NAME +
                ") VALUES (" +
                this.getId() + ", " +
                this.getType_id() + ", " +
                this.getPlace_id() + ",\'" +
                this.getDescription().replace("'", "''").replace("\\", "/") + "\',\'" +
                this.getInv().replace("'", "''").replace("\\", "/") + "\'," +
                this.getStation_id() +
                ");";
        DBManager.execute(query);
    }

    public static void runSeeder(){
        Apparatus apparatus1=new Apparatus(1,3,3,"Rabochiy","0123456789",1);
        apparatus1.insert(apparatus1.getId());
        Apparatus apparatus2=new Apparatus(2,6,3,"Rabochiy","0123456789",1);
        apparatus2.insert(apparatus2.getId());
        Apparatus apparatus3=new Apparatus(3,5,3,"Rabochiy","0123456789",1);
        apparatus3.insert(apparatus3.getId());

        Apparatus apparatus4=new Apparatus(4,3,3,"Rabochiy","0123456789",2);
        apparatus4.insert(apparatus4.getId());
        Apparatus apparatus5=new Apparatus(5,7,3,"Rabochiy","0123456789",2);
        apparatus5.insert(apparatus5.getId());
        Apparatus apparatus6=new Apparatus(6,5,3,"Rabochiy","0123456789",2);
        apparatus6.insert(apparatus6.getId());

        Apparatus apparatus7=new Apparatus(7,1,3,"Rabochiy","0123456789",3);
        apparatus7.insert(apparatus7.getId());
        Apparatus apparatus8=new Apparatus(8,6,3,"Rabochiy","0123456789",3);
        apparatus8.insert(apparatus8.getId());
        Apparatus apparatus9=new Apparatus(9,5,3,"Rabochiy","0123456789",3);
        apparatus9.insert(apparatus9.getId());

        Apparatus apparatus10=new Apparatus(10,1,3,"Rabochiy","0123456789",4);
        apparatus10.insert(apparatus10.getId());
        Apparatus apparatus11=new Apparatus(11,7,3,"Rabochiy","0123456789",4);
        apparatus11.insert(apparatus11.getId());
        Apparatus apparatus12=new Apparatus(12,5,3,"Rabochiy","0123456789",4);
        apparatus12.insert(apparatus12.getId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public int getPlace_id() {
        return place_id;
    }

    public void setPlace_id(int place_id) {
        this.place_id = place_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInv() {
        return inv;
    }

    public void setInv(String inv) {
        this.inv = inv;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }
}
