package dbmanager.tables;

import dbmanager.DBManager;
import dbmanager.tables.base.Table;

public class Communication extends Table {
    public static final String TABLE_NAME = "COMMUNICATON";

    public static final String ID_COL_NAME = "id";
    public static final String TYPE_ID_COL_NAME = "type_id";
    public static final String IP_COL_NAME = "IPAddress";
    public static final String DESCRIPTION_COL_NAME = "Description";

    private int id;
    private int type_id;
    private String ip;
    private String description;

    public Communication() {
    }

    public Communication(int type_id, String ip, String description) {
        this.type_id = type_id;
        this.ip = ip;
        this.description = description;
    }

    public Communication(int id, int type_id, String ip, String description) {
        this.id = id;
        this.type_id = type_id;
        this.ip = ip;
        this.description = description;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public boolean createTable() {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL_NAME + " serial PRIMARY KEY," +
                TYPE_ID_COL_NAME + " integer," +
                IP_COL_NAME + " text," +
                DESCRIPTION_COL_NAME + " text" +
                ");";
        DBManager.execute(query);
        return isExistsTable();
    }

    @Override
    public boolean isExistsTable() {
        String query = "SELECT * FROM " + TABLE_NAME + " LIMIT 1";
        return DBManager.executeQuery(query) != null;
    }

    @Override
    public void insert() {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                TYPE_ID_COL_NAME + "," +
                IP_COL_NAME + "," +
                DESCRIPTION_COL_NAME +
                ") VALUES (" +
                this.getType_id() + ", \'" +
                this.getIp().replace("'", "''").replace("\\", "/") + "\',\'" +
                this.getDescription().replace("'", "''").replace("\\", "/") + "\'" +
                ");";
        DBManager.execute(query);
    }

    public void insert(int id) {
        String query = "INSERT INTO " + this.getTableName() + " (" +
                ID_COL_NAME + "," +
                TYPE_ID_COL_NAME + "," +
                IP_COL_NAME + "," +
                DESCRIPTION_COL_NAME +
                ") VALUES (" +
                id + "," +
                this.getType_id() + ", \'" +
                this.getIp().replace("'", "''").replace("\\", "/") + "\',\'" +
                this.getDescription().replace("'", "''").replace("\\", "/") + "\'" +
                ");";
        DBManager.execute(query);
    }

    public static void runSeeder(){
        Communication communication1=new Communication(1,1,"0.0.0.0","Online ishlayapdi, Tel nomer:");//Andijon
        communication1.insert(communication1.getId());
        Communication communication2=new Communication(2,1,"0.0.0.0","Online ishlayapdi, Tel nomer:");//Toshkent
        communication2.insert(communication2.getId());
        Communication communication3=new Communication(3,3,"0.0.0.0","Offline ishlayapdi, Tel nomer:");//Mingtut
        communication3.insert(communication3.getId());
        Communication communication4=new Communication(4,3,"0.0.0.0","Offline ishlayapdi, Tel nomer:");//Chimgan
        communication4.insert(communication4.getId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
