package dbmanager;

import dbmanager.tables.Apparatus_type;
import dbmanager.tables.base.Table;

import java.sql.*;

public class DBManager {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/";
    private static final String DB_NAME = "eqpresentability";
    private static final String DB_LOGIN = "postgres";
    private static final String DB_PASSWORD = "wolferiene";

    private static Connection conn = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;

    public static boolean connect() {
        boolean answer = false;
        if (conn == null) {
            try {
                conn = DriverManager.getConnection(DB_URL + DB_NAME, DB_LOGIN, DB_PASSWORD);
                statement = conn.createStatement();
                answer = true;
            } catch (SQLException e) {
                //e.printStackTrace();
                answer = false;
                //TODO Exception Form need show
            }
        } else {
            answer = true;
        }
        return answer;
    }

    public static ResultSet executeQuery(String query) {
        if (statement != null) {
            try {
                resultSet = statement.executeQuery(query);
            } catch (Exception e) {
                e.printStackTrace();
                resultSet = null;
                //TODO Exception form need show
            }
        } else {
            resultSet = null;
            //TODO Exception form need show
        }
        return resultSet;
    }

    public static boolean execute(String query) {
        boolean answer=false;
        if (statement != null) {
            try {
                answer = statement.execute(query);
            } catch (Exception e) {
                e.printStackTrace();
                answer= false;
                //TODO Exception form need show
            }
        } else {
            answer=false;
            //TODO Exception form need show
        }
        return answer;
    }

    public static void close() {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (Exception ex) {
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (Exception ex) {
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (Exception ex) {
            }
        }
        resultSet = null;
        statement = null;
        conn = null;
    }



    public static <T> void checkTable(Class<? extends Table> table) {
//        Apparatus_type a=new Apparatus_type();

            boolean answer=table.isExistsTable();
            if(answer){
                System.out.println(table.getTableName()+" exist");
            }
            else {
                System.out.println(table.getTableName()+" not exist. Creating...");
                if(table.createTable()){
                    Apparatus_type.runSeeder();
                    System.out.println("created");
                }else {
                    System.out.println("Not created");
                }
            }

    }
}
